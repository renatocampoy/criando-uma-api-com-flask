import pandas as pd
from flask import Flask
from flask_ngrok import run_with_ngrok

app = Flask(__name__)  # the name of the application package
run_with_ngrok(app)


@app.route("/")  # code to assign HomePage URL in our app to function home.
def home():
    """
  The entire line below must be written in a single line.
  """
    return "<h1>Hi!</h1><p>Api is in <a href=\"/index\">/index</a></p>"


@app.route("/index")  # code to assign Input URL in our app to function input.
def input():
    df = pd.read_csv("dados.csv");
    return df.to_json()


app.run()
